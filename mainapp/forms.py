from django import forms
from mainapp.models import *
from django.forms import ModelForm, Form
from django.contrib.auth.models import User
from django.views.generic.base import RedirectView
import re 
from django.contrib.auth import logout
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login

        
class SignUpForm(forms.ModelForm):

    """
    SignUpForm
    """
    password_confirm = forms.CharField(widget=forms.PasswordInput(
    ), label="password_confirm", max_length=50, error_messages={'required': "Please re enter your password."})
    
    contact = forms.RegexField(label="contact", max_length=15, regex=r'^[0-9]+$', error_messages={'invalid': "This value may contain only numbers"})
    class Meta:
        model = UserProfile
        fields = ('username','first_name','last_name','email', 'password', 'password_confirm', 'dob', 'contact', 'gender')
        
    def save(self):
        instance = super(SignUpForm, self).save(commit=False)
        instance.set_password(self.cleaned_data["password"])
        return instance.save()

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
               
        try:
            email = cleaned_data.get('email')
        except:
            email = ''
        if not email:
            msg = "This Field Is Required"
            self._errors['email'] = self.error_class([msg])

        try:
            password = cleaned_data.get('password')
        except:
            password = ''
        if not password:
            msg = "This Field Is Required"
            self._errors['password'] = self.error_class([msg])

        try:
            first_name = cleaned_data.get('first_name')
        except :
            first_name = ''

        if not first_name:
            msg = "This Field Is Required"
            self._errors['first_name'] = self.error_class([msg])

        try:
            last_name = cleaned_data.get('last_name')
        except :
            last_name = ''

        if not last_name:
            msg = "This Field Is Required"
            self._errors['last_name'] = self.error_class([msg])

        try:
            username = cleaned_data.get('username')
        except :
            username = ''

        if not username:
            msg = "This Field Is Required"
            self._errors['username'] = self.error_class([msg])

        try:
            password_confirm = cleaned_data.get('password_confirm')
        except :
            password_confirm = ''

        if not password_confirm:
            msg = "This Field Is Required"
            self._errors['password_confirm'] = self.error_class([msg])

        try:
            gender = cleaned_data.get('gender')
        except :
            gender = ''

        if not gender:
            msg = "This Field Is Required"
            self._errors['gender'] = self.error_class([msg])

        user = UserProfile.objects.filter(email=email).exists()
        if user:
            msg = "A User With Same Email Id is Already Registered."
            self._errors["email"] = self.error_class([msg])
            return self.cleaned_data

        if password != password_confirm:
            msg = "Both Passwords do not match"
            self._errors["password_confirm"] = self.error_class([msg])
        return self.cleaned_data
    

class LoginForm(forms.Form):
    """
    LoginForm
    """
    # class Meta:
    #     model = UserProfile
    
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        if not email:
            msg = "Please enter Email"
            self._errors["email"] = self.error_class([msg])

        if not password:
            msg = "Please enter password"
            self._errors["password"] = self.error_class([msg])

        if email and password:
            try:
                account = UserProfile.objects.get(email__iexact=email)
            except:
                msg = "Looks Like You Have Entered Wrong Email, User Not Found"
                self._errors["password"] = self.error_class([msg])
                return self.cleaned_data

            user = auth.authenticate(username=account.username, password=password)   
            
            if user is None or not user.is_active:
                msg = "Email or Password is Incorrect!"
                self._errors["password"] = self.error_class([msg])
                return self.cleaned_data

        return self.cleaned_data            


class ForgotPasswordForm(forms.Form):

    """
    Forgot password form
    """
    email = forms.CharField()

    def clean(self):
        cleaned_data = super(ForgotPasswordForm, self).clean()   

        try:
            email = cleaned_data.get('email')
        except:
            email = ''

        if not email:
            msg = "This Field Is Required"
            self._errors['email'] = self.error_class([msg])
            return self.cleaned_data
        
        try:
            user = UserProfile.objects.get(email=email)
        except:
            user = ''
        print user

        if not user:
            msg = "The email address does not exist"
            self._errors['email'] = self.error_class([msg])
            return self.cleaned_data
        
        return self.cleaned_data


class ResetPasswordForm(forms.Form):

    old_password = forms.CharField(widget=forms.PasswordInput)
    new_password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ResetPasswordForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(ResetPasswordForm, self).save(commit=False)
        return instance.save()

    def clean(self):
        cleaned_data = super(ResetPasswordForm, self).clean()
        old_password = cleaned_data.get('old_password')
        new_password = cleaned_data.get('new_password')
        confirm_password = cleaned_data.get('confirm_password')

        if not old_password:
            msg = "Please enter old password"
            self._errors["old_password"] = self.error_class([msg])
        
        if not new_password:
            msg = "Please enter new password"
            self._errors["new_password"] = self.error_class([msg])

        
        if confirm_password is None:
            msg = "Please confirm your new Password."
            self._errors["confirm_password"] = self.error_class([msg])
            return self.cleaned_data

        if not (new_password == confirm_password):
            msg = "Both Password Do Not Match"

        user1 = UserProfile.objects.get(email=self.user.email)
        if not user1.check_password(old_password):
            msg = "Old Password Does Not Match With User"
            self._errors["old_password"] = self.error_class([msg])

        return self.cleaned_data



class ChangePasswordForm(forms.Form):
    """
    Change Password Form
    """

    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()

        try:
            password = cleaned_data.get('password')
        except:
            password = ''
        print password

        if not password:
            msg = "This Field Is Required"
            self._errors["password"] = self.error_class([msg])
            return self.cleaned_data

        try:
            confirm_password = cleaned_data.get('confirm_password')
        except:
            confirm_password = ''
        print confirm_password

        if not confirm_password:
            msg = "Confirm Password Is Required"
            self._errors["confirm_password"] = self.error_class([msg])
            return self.cleaned_data

        if password == confirm_password:
            pass
        else:
            msg="Both Passwords do not match"

            self._errors["confirm_password"] = self.error_class([msg])
            return self.cleaned_data


class AddTagForm(forms.ModelForm):
    
    class Meta:
        model = Tag

    def save(self, commit=True):
        instance = super(AddTagForm, self).save(commit=False)
        return instance.save()
    
    def clean(self):
        cleaned_data = super(AddTagForm, self).clean()
        add_tag = cleaned_data.get('tag')
        if not add_tag:
            msg = "please enter tag name"
            self._errors["tag"] = self.error_class([msg])
            return self.cleaned_data

        user = Tag.objects.filter(tag=add_tag).exists()
        if user:
            msg = "A Same Tag name is Already exist."
            self._errors["tag"] = self.error_class([msg])
            return self.cleaned_data
        return self.cleaned_data

class EditTagForm(forms.ModelForm):
    
    class Meta:
        model = Tag

    def save(self, commit=True):
        instance = super(EditTagForm, self).save(commit=False)
        return instance.save()
    
    def clean(self):
        cleaned_data = super(EditTagForm, self).clean()
        edit_tag = cleaned_data.get('tag')

        if not edit_tag:
            msg = "please enter edit_tag name"
            self._errors["edit_tag"] = self.error_class([msg])
                    
        return self.cleaned_data

class DeleteTagForm(forms.ModelForm):
    
    class Meta:
        model = Tag
        

class AddTransactionForm(forms.ModelForm):

    date = forms.DateField(label="date.", error_messages={
                                 'required': "Please enter your date."})
    t_type = forms.CharField(label="t_type", error_messages={
                               'required': "Please enter your t_type."})
    amt = forms.FloatField(label="amt", error_messages={'required': "Please enter your amt."})
    desc = forms.CharField(label="description", error_messages={'required': "Please enter your description."})

    class Meta:
        model = Transaction

    def save(self, commit=True):
        instance = super(AddTransactionForm, self).save(commit=False)
        return instance.save()

    def  clean(self):
        cleaned_data = super(AddTransactionForm, self).clean()
        return self.cleaned_data


class EditTransactionForm(forms.ModelForm):
    """
    Edit Transaction  Form 
    """
    class Meta:                                       
        model = Transaction
        
              
    def save(self, commit=True):
        instance = super(EditTransactionForm, self).save(commit=False)
        return instance.save()   

    def clean(self):
        cleaned_data = super(EditTransactionForm, self).clean()
        date = cleaned_data.get('date')
        # Exp = cleaned_data.get('Exp')
        tag = cleaned_data.get('tag')
        t_type = cleaned_data.get('t_type')
        acc_type = cleaned_data.get('acc_type')
        amt = cleaned_data.get('amt')
        desc = cleaned_data.get('desc')
        return self.cleaned_data     


class AddAccountForm(forms.ModelForm):
    """
    Add Account Form 
    """
    acc_no = forms.IntegerField(label="Account No.", error_messages={
                                 'required': "Please enter your Account No."})
    acc_name = forms.CharField(label="Account Name", max_length=25, error_messages={
                                 'required': "Please enter your Account name."})
    inst_name = forms.CharField(label="Institute", max_length=25, error_messages={
                                'required': "Please enter your Bank Name."})
    balance = forms.FloatField(label="Balance", error_messages={
                               'required': "Please enter your Balance."})
    
    class Meta:
        model = Account
        fields = ('name', 'acc_no', 'acc_name', 'inst_name', 'balance', 'acc_type', 'status_type')

    def save(self , commit=True):
        instance = super(AddAccountForm, self).save(commit=False)
        return instance.save()

    def  clean(self):
        cleaned_data = super(AddAccountForm, self).clean()             
        acc_no = cleaned_data.get('acc_no')
        acc_name = cleaned_data.get('acc_name')
        inst_name = cleaned_data.get('inst_name')
        balance = cleaned_data.get('balance')
        acc_type = cleaned_data.get('acc_type')
        status_type = cleaned_data.get('status_type')
        name = cleaned_data.get('name')
        user = Account.objects.filter(acc_no=acc_no).exists()
        if user:
            msg = "A User With Same Account No. is Already Registered."
            self._errors["acc_no"] = self.error_class([msg])
            return self.cleaned_data

        return self.cleaned_data


class EditAccountForm(forms.ModelForm):
    """
    Edit Account Form 
    """
    acc_no = forms.IntegerField(label="Account No.", error_messages={
                                 'required': "Please enter your Account No."})
    acc_name = forms.CharField(label="Account Name", max_length=25, error_messages={
                                 'required': "Please enter your Account name."})
    inst_name = forms.CharField(label="Institute", max_length=25, error_messages={
                                'required': "Please enter your Bank Name."})
    balance = forms.FloatField(label="Balance", error_messages={
                               'required': "Please enter your Balance."})

    class Meta:                                       
        model = Account
        fields = ('name', 'acc_no', 'acc_name', 'inst_name', 'balance', 'acc_type', 'status_type')
              
    def save(self, commit=True):
        instance = super(EditAccountForm, self).save(commit=False)
        return instance.save()   

    def clean(self):
        cleaned_data = super(EditAccountForm, self).clean()
        acc_no = cleaned_data.get('acc_no')
        acc_name = cleaned_data.get('acc_name')
        inst_name = cleaned_data.get('inst_name')
        balance = cleaned_data.get('balance')
        acc_type = cleaned_data.get('acc_type')
        status_type = cleaned_data.get('status_type')
        name = cleaned_data.get('name')
        user = Account.objects.filter(acc_no=acc_no).exists()
        if user:
            msg = "A User With Same Account No. is Already Registered."
            self._errors["acc_no"] = self.error_class([msg])
            return self.cleaned_data

        return self.cleaned_data     
