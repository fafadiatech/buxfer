from django.db import models
from uuid import uuid4
from django.conf import settings
from django.contrib.auth.models import User


class UserProfile(User):

	dob = models.DateField()
	phone = models.IntegerField(max_length=12, blank=True, null=True)
	GENDER_CHOICES = (('Male', 'Male'), ('Female', 'Female'))
	gender = models.CharField(choices=GENDER_CHOICES, max_length=10)
	profilepic = models.ImageField(upload_to='profile_pictures', blank=True, null=True)
	activation_key = models.CharField(max_length=40, blank=True)
	contact = models.IntegerField(max_length=12, blank=True, null=True)
	def __unicode__(self):
		return self.username
	
	def is_activation_key_exists(self, activation_key):
		"""
		Check existance of activation key
		"""
		try:
		    userprofile = UserProfile.objects.get(activation_key=activation_key)

		except:
		    return None
		return "activation key exists"

class Account(models.Model):
	
	acc_no = models.IntegerField(max_length=20, null=False)	
	acc_name = 	models.CharField(max_length=255)
	name = models.ForeignKey(UserProfile)
	inst_name = models.CharField(max_length=125)
	balance = models.FloatField()
	acc_type = models.CharField(max_length=255, choices=settings.ACCOUNT_CHOICES)
	status_type = models.CharField(choices=settings.STATUS_CHOICES,max_length=25)

	def __unicode__(self):
		return "%s %s" %(self.name.first_name , self.name.last_name) 


class Tag(models.Model):
	tag = models.CharField(max_length=25)
	user = models.ForeignKey(User)
	def __unicode__(self):
		return self.tag

class Transaction(models.Model):
	account = models.ForeignKey(Account)
	date = models.DateField()
	t_type = models.CharField(max_length=255, choices=settings.TRANSACTION_CHOICES)
	desc = models.CharField(max_length=225)
	amt = models.FloatField()
	add_tag = models.ForeignKey(Tag)
	# new_balance = models.ForeignKey(Account)

	def __unicode__(self):
		return self.t_type

class ForgotPassword(models.Model):
	user = models.ForeignKey(UserProfile, blank=True, null= True)
	hashkey= models.CharField(max_length= 200)
	timestamp = models.DateField()

	def __unicode__(self):
		return self.hashkey
	

	
