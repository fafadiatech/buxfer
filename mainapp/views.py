import datetime
import json
from datetime import timedelta
from uuid import uuid4
from django.conf import settings
from django.views.generic import *
from django.core.mail import send_mail
from django.contrib import auth, messages
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from braces.views import LoginRequiredMixin
from mainapp.forms import *
from mainapp.models import *



class IndexView(TemplateView):
	template_name = "mainapp/main.html"


class EditTransactionsView(TemplateView):

	template_name = "mainapp/edit_transaction.html"


class SignupView(FormView):
	"""
	Signup page
	"""
	template_name = "mainapp/signup.html"
	form_class = SignUpForm

	def form_valid(self, form):
		email = form.cleaned_data['email']
		form.save()
		user = UserProfile.objects.get(email=email)
		username = user.username
		subject = "activation link for signup "
		activation_key = str(uuid4())[:10]
		user.activation_key = activation_key
		user.is_active = True
		user.save()
		send_mail(subject, settings.SITE_NAME  +'activate/'+activation_key, settings.EMAIL_HOST_USER, [email], fail_silently=False)
		msg = "Your Signup is complete, Check your email to activate your account."
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
	    form_class = self.get_form_class()
	    form = self.get_form(form_class)
	    if form.is_valid():
	        return self.form_valid(form)
	    else:
	        return self.form_invalid(form)



class ActivateAccView(TemplateView):
    """
    Account Activation
    """
    def get_context_data(self, **kwargs):
        userprofile = UserProfile()
        activation_key = kwargs['activation_key']
        
        if not userprofile.is_activation_key_exists(activation_key):
            self.template_name = "404.html"
        else:
            self.template_name = "mainapp/activate.html"

        return activation_key




class LoginView(FormView):
	"""
	Login page
	"""
	template_name = "mainapp/login.html"
	form_class = LoginForm
	success_url = "/"

	def form_valid(self, form):
		email = form.cleaned_data['email']
		password = form.cleaned_data['password']
		u = UserProfile.objects.get(email=email)
		user = authenticate(username=u.username, password=password)
		
		if user is not None:
			if user.is_active:
				login(self.request, user)
				return HttpResponseRedirect(self.get_success_url())
			else:
				return self.form_invalid(form)
		else:
			return self.form_invalid(form)
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			
			return self.form_valid(form)
		else:
			
			return self.form_invalid(form)	



class ForgotPasswordView(FormView):	
	"""
	Forgot Password Request Form (entering email id)
	"""
	template_name = "mainapp/forgotpassword.html"
	form_class = ForgotPasswordForm

	def get_context_data(self, **kwargs):
		context = super(ForgotPasswordView, self).get_context_data(**kwargs)
		return context

	def form_valid(self, form):
		email = form.cleaned_data['email']
		user = UserProfile.objects.get(email=email)
		username = user.username
		ForgotPassword.objects.filter(user=user).delete()
		hashkey = str(uuid4())[:15]
		ForgotPassword.objects.create(user=user , hashkey= hashkey, timestamp= datetime.date.today())
		send_mail(username, settings.SITE_NAME +'forgotpassword/'+hashkey , settings.EMAIL_HOST_USER , [email] , fail_silently=False)
		msg = "An email has been sent on your registered email id for further assistance on changing your password"
		messages.success(self.request, msg)
		
		return self.render_to_response(self.get_context_data(form=form))
		
	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))
	
	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)



class ForgotPasswordValidationView(RedirectView):
	model = ForgotPassword
	permanent = False
	query_string = True
	error = '/display_error/'


	def get_redirect_url(self, *args, **kwargs):
		url_hashkey = self.kwargs['hashkey']
		obj = get_object_or_404( ForgotPassword, hashkey= url_hashkey)
		if (obj.timestamp < (datetime.date.today() - timedelta(days=2))):
			ForgotPassword.objects.filter(hashkey = self.kwargs['hashkey']).delete()
			return reverse("404error")
		else:
			self.url = '/changepassword/%s/' % (url_hashkey)
			return super(ForgotPasswordValidationView,  self).get_redirect_url(*args, **kwargs)



	
class ChangePasswordView(FormView):
	template_name = "mainapp/changepassword.html"
	form_class = ChangePasswordForm
	success_url= "/"
	
	def form_valid(self, form, **kwargs):
		password= form.cleaned_data['password']
		print self.kwargs['hashkey']
		obj = ForgotPassword.objects.get(hashkey = self.kwargs['hashkey'])
		user = UserProfile.objects.get(pk=obj.user_id)
		user.set_password(password)
		user.save()
		ForgotPassword.objects.filter(hashkey = self.kwargs['hashkey']).delete()
		msg = "Your password has been successfully changed."
		messages.success(self.request, msg)

		return self.render_to_response(self.get_context_data(form=form)) 

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		print form.errors
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

class ErrorDisplayView(TemplateView):

	template_name = "mainapp/link_expired.html"



class SignOutView(RedirectView):

	def get_redirect_url(self):
		logout(self.request)
		return reverse('index')



class ResetPasswordView(FormView):

    template_name = "mainapp/reset_password.html"
    form_class = ResetPasswordForm

    def form_valid(self, form):
        confirm_password = form.cleaned_data['confirm_password']
        current_user = self.request.user
        current_user.set_password(confirm_password)
        current_user.save()
        sub = "Reset password link "
        user_email = current_user.email
        send_mail(sub, settings.SITE_NAME  +'login/', settings.EMAIL_HOST_USER, [user_email], fail_silently=False)
        msg = "Your Password is Successfully Reset, Check Your Email For Confirmation"
        messages.success(self.request, msg)
        return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = form_class(request.POST, user=request.user)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
        return activation_key 


class TransactionsView (TemplateView):

	template_name = "mainapp/transactions.html"


class EditTransactionsView (TemplateView):

	template_name = "mainapp/edit_transaction.html"


class AccountDetailsView (TemplateView):

	template_name = "mainapp/account_detail.html"



class AccountView (LoginRequiredMixin, TemplateView , FormView):

	"""
	Add AccountView
	"""
	login_url = "/login/"
	template_name = "mainapp/account.html"
	form_class = AddAccountForm
	success_url = "/"

	def get_context_data(self, **kwargs):
		context = super(AccountView, self).get_context_data(**kwargs)
		context['objects'] = Account.objects.all() 
		return context

	def form_valid(self, form):
		form.save()
		msg = "Account added Successfully"
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))
		
	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
				
		if form.is_valid():
			return self.form_valid(form)
		else:
			print form.errors
		return self.form_invalid(form)


class EditAccountView(LoginRequiredMixin, UpdateView, FormView):
	"""
	Update User Account Info
	"""
	model = Account
	login_url = "/login/"
	template_name = "mainapp/edit_account.html"
	success_url = "mainapp/account.html"
	# form_class = EditAccountForm

	def get_object(self, queryset=None):
		return get_object_or_404(Account, object=self.kwargs['object'])

	def form_valid(self, form):
		form.save()
		msg = "Account is successfully updated."
		messages.success(self.request, msg)
		current_user = self.request.user 
		return HttpResponseRedirect (reverse(''))

# 	def form_valid(self, form):
# 		form.save()
# 		msg = "Account is successfully updated."
# 		messages.success(self.request, msg)
# 		return redirect(self.success_url())



class TagView (TemplateView):

	template_name = "mainapp/tags.html"		


class AddTagView(FormView):

	template_name = "mainapp/tags.html"
	form_class = AddTagForm

	def get_context_data(self, **kwargs):
		context = super(AddTagView, self).get_context_data(**kwargs)
		context['objects'] = Tag.objects.all() 
		return context


	def form_valid(self, form):
		form.save()
		user = self.request.user
		print user
		msg = "tag added successfully"
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))


	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		print "form", form.is_valid()
		if form.is_valid():
			return self.form_valid(form)
		else:
			print form.errors
			return self.form_invalid(form)


class EditTagView(FormView, TemplateView):

	template_name = "mainapp/edit_tags.html"
	form_class = EditTagForm
	success_url = "/add_tags/"

	def get_context_data(self, **kwargs):
		context = super(EditTagView, self).get_context_data(**kwargs)
		context['tag_id'] = kwargs['tag_id']
		tag_obj = Tag.objects.get(id=kwargs['tag_id'])
		context['tag_name'] = tag_obj.tag
		return context

	def form_invalid(self, form):
		print form.errors
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			tagid =  kwargs['tag_id']
			tag = form.cleaned_data['tag']
			print tag
			updated_tag = Tag.objects.filter(id=tagid).update(tag=tag)
			msg ="tag edit done"
			messages.success(self.request, msg)
			return self.form_valid(form)
			# return self.render_to_response(self.get_context_data(form=form, tag_id))
		else:
			return self.form_invalid(form)


class DeleteTagView(View):
	template_name = "mainapp/tags.html"
	class Meta:
		model = Tag

	def post(self, request, *args, **kwargs):
		transactionid = request.POST.get('tagid')
		Tag.objects.filter(id=int(transactionid)).delete()
		return HttpResponse(json.dumps({'status':1}))
	
	# def get_context_data(self, **kwargs):
	# 	context = super(EditTagView, self).get_context_data(**kwargs)
	# 	context['transactionid'] = kwargs['deletedId']
	# 	print transactionid
	# 	return context

	# def form_invalid(self, form):
	# 	return self.render_to_response(self.get_context_data(form=form))

class AddTransactionView(FormView, TemplateView):
	template_name = "mainapp/transactions.html"
	form_class = AddTransactionForm

	def get_context_data(self, **kwargs):
		context = super(AddTransactionView, self).get_context_data(**kwargs)
		context['trans_type']= settings.TRANSACTION_CHOICES
		context['tag'] = Tag.objects.all()
		context['transactions'] = Transaction.objects.all()
		context['account_choice']= settings.ACCOUNT_CHOICES
		context['account'] = Account.objects.all()
		return context


	def form_valid(self, form):
		print form.data
		# a = transactions.account.balance
		# current_bal = account.objects.get(balance=balance)
		form.save()
		msg = "add-transaction added successfully"
		messages.success(self.request, msg)
		return self.render_to_response(self.get_context_data(form=form))
		
	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class EditTransactionsView(FormView, TemplateView):

	template_name = "mainapp/edit_transaction.html"
	form_class = EditTransactionForm
	success_url = "/addtransaction/"

	def get_context_data(self, **kwargs):
		context = super(EditTransactionsView, self).get_context_data(**kwargs)
		context['transaction_id'] = kwargs['transaction_id']
		print context['transaction_id']
		transaction_obj = Transaction.objects.get(id=kwargs['transaction_id'])
		context['transaction'] =  Transaction.objects.get(id=kwargs['transaction_id'])
		context['tag'] = Tag.objects.all()
		context['trans_type']= settings.TRANSACTION_CHOICES
		context['account_choice']= settings.ACCOUNT_CHOICES
		return context

	def form_invalid(self, form):
		print form.errors
		return self.render_to_response(self.get_context_data(form=form))

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		print form.is_valid()
		if form.is_valid():
			transactionid = kwargs['transaction_id']
			date = form.cleaned_data['date']
			# updated_date = Transaction.objects.filter(id=transactionid).update(date=date)
			# updated_amt = Transaction.objects.filter(id=transactionid).update(amt=amt)
			# updated_amt = Transaction.objects.filter(id=transactionid).update(amt=amt)
			msg ="transaction edit done"
			messages.success(self.request, msg)
			return self.form_valid(form)
		else:
			return self.form_invalid(form)
