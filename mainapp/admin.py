from django.contrib import admin
from mainapp.models import UserProfile, Tag , Account , Transaction, ForgotPassword
# Register your models here.





admin.site.register(UserProfile)
admin.site.register(Account)
admin.site.register(Transaction)
admin.site.register(Tag)
admin.site.register(ForgotPassword)
