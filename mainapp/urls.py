from django.conf.urls import patterns, include, url
from mainapp.views import *
from mainapp.forms import *

urlpatterns = patterns('',
	url(
		regex= r'^$',
		view= IndexView.as_view(),
		name="index"
		),
	url(
		regex= r'^deletetag/$',
		view= DeleteTagView.as_view(),
		name="tag_delete"
		),

	url(
		regex= r'^forgotpassword/$', 
		view= ForgotPasswordView.as_view(),
		name="forgot_password"
		),
	url(
		regex=r'^reset_password/$',
		view=ResetPasswordView.as_view(),
		name='resetpassword'
		),
	url( 
		regex= r'^forgotpassword/(?P<hashkey>[-\w\d]+)/$',
		view= ForgotPasswordValidationView.as_view(),
		name="forgotpasswordvalidation"   
		),
	url(
		regex= r'^changepassword/(?P<hashkey>[-\w]+)/$',
		view= ChangePasswordView.as_view(),
		name="change_password"
		),
	url(
		regex= r'^display_error/',
		view= ErrorDisplayView.as_view(),
		name="404error"
		),
	url(
		regex= r'^signup/$',
		view= SignupView.as_view(),
		name="signup"
		),
	url(
		regex= r'^login/$',
		view= LoginView.as_view(),
		name="login"   
		),
	url(
		regex=r'^signout/$', 
		view=SignOutView.as_view(),
		name='signout'
		),
	url(
		regex=r'activate/(?P<activation_key>[-\w]+)$',
		view=ActivateAccView.as_view(),
		name='activate-account'
		),
	url(
		regex= r'^detail_account/$',
		view= AccountDetailsView.as_view(),
		name="detail_account"
		),
	url(
		regex=r'edit_account/$',
		view= EditAccountView.as_view(),
		name='edit_account'
		),
	url(
		regex=r'account/$',
		view= AccountView.as_view(),
		name='account'
		),
	url(
		regex= r'^tag_edit/(?P<tag_id>\d+)$',
		view= EditTagView.as_view(),
		name="tag_edit"
		),
	
	url(
		regex= r'^add_tags/$',
		view= AddTagView.as_view(),
		name="add_tags"
		),

	url(
		regex= r'^transaction_edit/(?P<transaction_id>\d+)$',
		view= EditTransactionsView.as_view(),
		name="transaction_edit"
		),
	url(
		regex= r'^transaction/$',
		view= AddTransactionView.as_view(),
		name="addtransaction"
		),
	url(
		regex= r'^transaction_edit/$',
		view= EditTransactionsView.as_view(),
		name="transaction_edit"
		),
	)
