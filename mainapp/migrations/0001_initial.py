# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('acc_no', models.IntegerField(max_length=20)),
                ('acc_name', models.CharField(max_length=255)),
                ('inst_name', models.CharField(max_length=125)),
                ('balance', models.FloatField()),
                ('acc_type', models.CharField(max_length=25, choices=[(b'Checking Account', b'Checking Account'), (b'Savings Account', b'Savings Account'), (b'Salary Account', b'Salary Account'), (b'Demat Account', b'Demat Account')])),
                ('status_type', models.CharField(max_length=25, choices=[(b'Active', b'Active'), (b'Closed', b'Closed')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ForgotPassword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hashkey', models.CharField(max_length=200)),
                ('timestamp', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('t_type', models.CharField(max_length=25, choices=[(b'Expense', b'Expense'), (b'Income', b'Income'), (b'Transfer', b'Transfer')])),
                ('desc', models.CharField(max_length=225)),
                ('amt', models.FloatField()),
                ('account_type', models.ForeignKey(to='mainapp.Account')),
                ('add_tag', models.ForeignKey(to='mainapp.Tag')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('dob', models.DateField()),
                ('phone', models.IntegerField(max_length=12, null=True, blank=True)),
                ('gender', models.CharField(max_length=10, choices=[(b'Male', b'Male'), (b'Female', b'Female')])),
                ('profilepic', models.ImageField(null=True, upload_to=b'profile_pictures', blank=True)),
                ('activation_key', models.CharField(max_length=40, blank=True)),
                ('contact', models.IntegerField(max_length=12, null=True, blank=True)),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
        ),
        migrations.AddField(
            model_name='forgotpassword',
            name='user',
            field=models.ForeignKey(blank=True, to='mainapp.UserProfile', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='account',
            name='name',
            field=models.ForeignKey(to='mainapp.UserProfile'),
            preserve_default=True,
        ),
    ]
