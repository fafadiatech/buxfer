from django.conf.urls import patterns, include, url
from django.contrib import admin
from mainapp.views import *
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    url(r'^admin/', include(admin.site.urls)),
	url(r'^', include('mainapp.urls')),
	
	
	)