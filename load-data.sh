#!/bin/bash

read -p "Your Virtual Environment is Activated ? Do You Want to Load Data ? (yes or no)" ans

if [ "$ans" == "yes" ] || [ "$ans" == "YES" ];
	then
	python manage.py loaddata fixtures/profiles.json
	python manage.py loaddata fixtures/userprofiles.json
	python manage.py loaddata fixtures/newtag.json
	# python manage.py loaddata fixtures/schoolchoices.json
	# python manage.py loaddata fixtures/sites.json
	# python manage.py loaddata fixtures/socialapp.json 
	# python manage.py loaddata fixtures/profiles.json 
	# python manage.py loaddata fixtures/tutor-profile.json 
	# python manage.py loaddata fixtures/course-category.json 
	# python manage.py loaddata fixtures/course-subcategory.json 
	# python manage.py loaddata fixtures/course-prerequisite.json 
	# python manage.py loaddata fixtures/course-target-audience.json 
	# python manage.py loaddata fixtures/course-whats-inside.json  
	# python manage.py loaddata fixtures/course-goingto-get-from.json 
	# python manage.py loaddata fixtures/course-course.json 
	# python manage.py loaddata fixtures/course-media.json 
	# python manage.py loaddata fixtures/course-section.json 
	# python manage.py loaddata fixtures/course-session.json
	# python manage.py loaddata fixtures/subscription-plan.json 
	# python manage.py loaddata fixtures/interview-questions.json
	if [ $? = 0 ];
		then
		echo ""
		echo "Data Loaded"
		echo ""
		exit 0
	else
		echo ""
		echo "Error Loading Data"
		echo ""
		exit 1
	fi
elif [ "$ans" == "no" ] || [ "$ans" == "NO" ];
	then
	echo ""
	echo "Ok...Bye...!!!"
	echo ""
	exit 0
else
	echo ""
	echo "Valid Choices are YES||yes, NO||no"
	echo ""
	exit 1
fi